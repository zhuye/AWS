# AWS系列课程

## 讲师介绍

朱烨，拥有TW最简洁而亲切的称呼：叔。其15年的运维行业经历本身就是一部中国互联网的发展演化史。具有丰富的网络，系统等运维相关的技术知识，持有CCIE，AWS解决方案架构师认证。

## 课程宣言

今天，云服务不再是高高在上，缥缈而虚幻的一个概念，它已经深入到我们生活中的每个方方面面。AWS作为云服务的行业领头羊，拥有完整的服务体系和生态圈，成为各行业线上业务服务提供商的首选。AWS系列课程覆盖了从基础到高级的不同层次知识点，不同技术背景的小伙伴可以根据情况选择参与相关课程，了解AWS相关的知识。有兴趣的同学们赶快来，让叔带你们直飞云端。

## 课程内容

### 初级

##### 适用人群

1. 零基础，想了解AWS常用服务和相关用途。
2. 有基础计算机知识，想学习AWS常用服务的基础管理。

##### 课程内容

* AWS基础知识和服务介绍
* IAM基础
* VPC/NACL/
* EC2/Security Group/EBS/EIP
* S3/EFS/Glacier
* RDS/DynamoDB/ElastiCache
* Route53
* SQS/SNS/SES
* CloudWatch
* Billing/Trusted Advisor 

### 中级

##### 适用人群

1. 有AWS基础服务相关知识，想进一步学习AWS的高级管理技术。
2. 备考AWS助理架构师或系统管理认证人员。

##### 课程内容

* AWS CLI
* VPC深入管理
* EC2深入管理
* S3深入管理
* IAM深入管理/CloudTrail
* ELB/ASG
* WAF/Shield
* Certificate Manager/KMS
* API Gateway
* CloudFront
* CloudFormation

### 高级

##### 适用人群

1. 中级运维技术人员，想学习如何在AWS上做高级的运维管理。

##### 课程内容

- 高可用/业务持续
- 账号管理
- 部署管理
- 网络设计
- 数据存储
- 安全
- 弹性伸缩
- 云迁移/混合云